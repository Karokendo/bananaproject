﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
	public Rigidbody body;
	public float movementSpeed = 1.0f;
    public float jumpPower = 1.0f;
	bool onGround = false;

	public string xAxisCode = "Horizontal";
	public string yAxisCode = "Vertical";
	public string jumpAxisCode = "Fire1";
    public string powerAxisCode = "Fire2";

    public Transform cast;
    public GameObject Flame;
    public GameObject particles;
    public Collider smashCollision;

    bool smash = true;

    void Start()
    {
        if (body == null)
            body = GetComponent<Rigidbody>(); 
    }

    Vector3 lastInput;

	private void Update()
	{
		body.WakeUp();

		Vector3 input = new Vector3(Input.GetAxisRaw(xAxisCode), 0, Input.GetAxisRaw(yAxisCode)).normalized;
		if ((input.x != 0 || input.z != 0))
			lastInput = input;


        if (Input.GetButton("Fire2"))
            Flame.gameObject.SetActive(true);      
        else
            Flame.gameObject.SetActive(false);


        if (Input.GetButtonDown("Fire3") && smash)
        {
            body.AddForce(Vector3.down * (jumpPower * 4) * Time.fixedDeltaTime);

            Instantiate(particles, body.transform.position, Quaternion.identity);
        }

    }

	// Update is called once per frame
	void FixedUpdate()
    {
		

		if (onGround)
		{	
			body.AddForce(lastInput * movementSpeed * Time.fixedDeltaTime);
		}
		body.rotation = Quaternion.Euler(0, Vector3.Angle(-Vector3.forward, lastInput) * (lastInput.x > 0 ? -1 : 1), 0);
    }


    private void OnCollisionStay(Collision col)
    {
        if (col.gameObject.tag == "Ground")
        {
            smash = false;

            if (onGround = Input.GetButton("Fire1"))
            {
                smash = true;
                body.AddForce(Vector3.up * jumpPower * Time.fixedDeltaTime);
            }

        }
    }

}