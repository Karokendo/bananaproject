﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject player;
    Vector3 pos;
    public Vector3 margin;

    public float cameraSpeed = 0.1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        pos.x = Mathf.Lerp(transform.position.x, player.transform.position.x + margin.x, cameraSpeed);
        pos.y = Mathf.Lerp(transform.position.y, player.transform.position.y + margin.y, cameraSpeed);
        pos.z = Mathf.Lerp(transform.position.z, player.transform.position.z + margin.z, cameraSpeed);

        transform.position = pos;
    }
}
